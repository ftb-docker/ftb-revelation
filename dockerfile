FROM openjdk:alpine
COPY . /opt
WORKDIR /opt/minecraft

# Updating the version zip requires a similar change to the CI version tag
RUN wget https://media.forgecdn.net/files/2778/975/FTBRevelationServer_3.2.0.zip
RUN unzip FTBRevelationServer_3.2.0.zip

RUN chmod 700 FTBInstall.sh
RUN chmod 700 ServerStart.sh
RUN ./FTBInstall.sh
RUN cp /opt/eula.txt /opt/minecraft

RUN apk add --no-cache py-pip
RUN pip install mcstatus

HEALTHCHECK --interval=30s --timeout=5s --retries=5 \
   CMD mcstatus localhost ping || exit 1

EXPOSE 25565
ENTRYPOINT ["/opt/minecraft/ServerStart.sh"]
